package main

import (
	"context"
	"demo-kube/internal/routes"
	"demo-kube/internal/workload"
	"fmt"
	"github.com/d-kolpakov/logger"
	"github.com/dhnikolas/configo"
	_ "github.com/lib/pq"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const ServiceName = "demo-kube"

var AppVersion string

func main() {
	fmt.Println("Starting service " + ServiceName)

	logDriver := &logger.STDOUTDriver{}
	loggerConfig := logger.LoggerConfig{
		ServiceName: ServiceName,
		Level:       configo.EnvInt("logging-level", 2),
		Buffer:      configo.EnvInt("app-logger-buffer-size", 10000),
		Output:      []logger.LogDriver{logDriver},
	}
	l, err := logger.GetLogger(loggerConfig)
	if err != nil {
		panic(err)
	}

	ic := make(chan workload.Item, 5000)
	workloadContext, workloadCancel := context.WithCancel(context.Background())
	w := workload.New()
	exitChan := w.Start(workloadContext, ic)

	server := routes.New(ServiceName, AppVersion, l, ic)
	go func(s *http.Server) {
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed{
			panic(err)
		}
	}(server)

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)

	<-c

	fmt.Println("Shutdown ...")
	ctx, serverCancel := context.WithTimeout(context.Background(), 15 * time.Second)
	server.Shutdown(ctx)
	serverCancel()
	workloadCancel()

	<-exitChan

	fmt.Println("Successful shutdown")
}
