module demo-kube

go 1.12

require (
	github.com/armon/go-metrics v0.0.0-20190430140413-ec5e00d3c878 // indirect
	github.com/d-kolpakov/logger v1.1.0
	github.com/dhnikolas/configo v1.0.2
	github.com/fatih/color v1.7.0
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/hashicorp/go-msgpack v0.5.5 // indirect
	github.com/heptiolabs/healthcheck v0.0.0-20180807145615-6ff867650f40
	github.com/jackc/pgx/v4 v4.8.1
	github.com/lib/pq v1.3.0
	github.com/mailru/easyjson v0.7.0
	github.com/prometheus/client_golang v1.2.1 // indirect
	github.com/satori/go.uuid v1.2.0
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0 // indirect
)
